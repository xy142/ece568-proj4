package com.duke.hw4.service;

import org.w3c.dom.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.duke.hw4.Utils.XMLGenerator;
import com.duke.hw4.Utils.XMLParser;
import com.duke.hw4.domain.Account;
import com.duke.hw4.domain.Deal;
import com.duke.hw4.domain.Order;
import com.duke.hw4.domain.Position;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

// using XMLGenerator to generate create XML
// get root of transaction to generate transaction XML
// they have the different output data structure
@Service
public class XMLService {
  private final XMLParser xmlParser;

  // "created, sym="abc" id="1"
  private List<Pair<String, List<Pair<String, String>>>> result;

  private XMLGenerator xmlGenerator;

  private Element transactionRoot;

  private Document doc;

  @Autowired
  AccountService accountService;

  @Autowired
  SymbolService symbolService;

  @Autowired
  PositionService positionService;

  @Autowired
  OrderService orderService;

  @Autowired
  DealService dealService;

  XMLService(XMLParser xmlParser) {
    this.xmlParser = xmlParser;
    result = new ArrayList<>();
    xmlGenerator = new XMLGenerator(result);
    transactionRoot = new Element("results");
    doc = new Document(transactionRoot);
  }

  /**
   * This function is to parse XML file
   * 
   * @throws Exception
   */

  @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 36000, rollbackFor = Exception.class)
  public void parseXML(String s) throws Exception {
    System.out.println(s);
    org.w3c.dom.Document xml = this.xmlParser.getDocument(s);
    xml.getDocumentElement().normalize();
    Node rootNode = xml.getDocumentElement();
    String tag = xml.getDocumentElement().getNodeName();
    System.out.println(tag);

    if (tag.equals("create")) {
      parseCreate(rootNode);
      xmlGenerator.generateCreateResultsXML();
    } else {
      parseTransaction(rootNode);
      xmlGenerator.generateTransactionXML(doc);
    }
  }

  /**
   * This function is to parse <create>
   * 
   * @param nodes the child nodes of <create>
   * @throws TransformerException
   * @throws ParserConfigurationException
   * @throws IOException
   */
  public void parseCreate(Node rootNode) throws IOException {
    NodeList childNodes = rootNode.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      Node node = childNodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("account")) {
        parseCreateAccount(node);
      } else if (node.getNodeType() == Node.ELEMENT_NODE && node.getNodeName().equals("symbol")) {
        parseSymbol(node);
      }
    }
    // TODO Every time handle one request, generate a response
  }

  /**
   * This funciton is to parse Transaction
   * 
   * @param rootNode xml node <Transaction>
   * @throws Exception
   */
  public void parseTransaction(Node rootNode) {
    String strAccountNumber = getAttributeValue(rootNode, "id");
    if (isNumeric1(strAccountNumber) || rootNode.getAttributes().getLength() != 1
        || rootNode.getAttributes().getNamedItem("id") == null) {
      // TODO transcation root node not in the correct format
    }

    Long accountNumber = Long.parseLong(getAttributeValue(rootNode, "id"));
    if (!accountService.checkAccountExists(accountNumber)) {
      // TODO account ID is invalid, then an <error> will be reported for each
      // transaction
      System.out.println("Transaction error: account not exist");
      return;
    }

    System.out.println(accountNumber);
    NodeList childNodes = rootNode.getChildNodes();
    for (int i = 0; i < childNodes.getLength(); i++) {
      Node node = childNodes.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        String nodeName = node.getNodeName();
        // <order> tag
        if (nodeName.equals("order")) {
          parseOrder(accountNumber, node);
        } else if (nodeName.equals("cancel")) { // <cancel> tag
          parseTransactionCancel(accountNumber, node);
        } else if (nodeName.equals("query")) { // <query> tag
          parseQuery(accountNumber, node);
        } else {
          // TODO: invalid tage
        }
      }
    }
  }

  /**
   * This function is to parse <symbol>
   * 
   * @param node <symbol> node
   */
  public void parseSymbol(Node node) {
    String symbol = getAttributeValue(node, "sym");

    // if symbol not exist, create such symbol
    symbolService.saveSymbol(symbol);

    NodeList nodeList = node.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node currNode = nodeList.item(i);
      if (currNode.getNodeType() == Node.ELEMENT_NODE) {
        parseCreateSymbolToAccount(symbol, currNode);
      }
    }
  }

  /**
   * This function parses to create symbol, add shares to account
   * 
   * @param node xml node
   */
  public void parseCreateSymbolToAccount(String symbol, Node node) {
    // TODO check format
    Long accountNumber = Long.parseLong(getAttributeValue(node, "id"));
    Integer shares = Integer.parseInt(node.getTextContent());

    List<Pair<String, String>> innerList = new ArrayList<>();
    Pair<String, String> inner1 = new ImmutablePair<String, String>("sym", symbol);
    Pair<String, String> inner2 = new ImmutablePair<String, String>("id", Long.toString(accountNumber));
    innerList.add(inner1);
    innerList.add(inner2);

    if (!accountService.checkAccountExists(accountNumber)) {
      // TODO
      System.err.println("Account not exist, Symbol added to that account failed");
      Pair<String, List<Pair<String, String>>> outer = new ImmutablePair<String, List<Pair<String, String>>>(
          "Account not existed", innerList);
      result.add(outer);
      return;
    }
    System.out.println(accountNumber + "   " + shares);
    Position position = new Position();
    position.setAccountNumber(accountNumber);
    position.setShares(shares);
    position.setSymbol(symbol);
    positionService.createSymbolToAccount(accountNumber, symbol, shares);

    Pair<String, List<Pair<String, String>>> outer = new ImmutablePair<String, List<Pair<String, String>>>("created",
        innerList);
    result.add(outer);
  }

  /**
   * This function parses to create new account
   * 
   * @param node xml node
   * @throws TransformerException
   * @throws ParserConfigurationException
   */

  public void parseCreateAccount(Node node) {
    // TODO check format
    Long accountNumber = Long.parseLong(getAttributeValue(node, "id"));
    Double balance = Double.parseDouble(getAttributeValue(node, "balance"));
    Account account = new Account();
    account.setAccountNumber(accountNumber);
    account.setBalance(balance);

    List<Pair<String, String>> innerList = new ArrayList<>();
    Pair<String, String> inner = new ImmutablePair<String, String>("id", Long.toString(accountNumber));
    innerList.add(inner);

    if (!accountService.checkAccountExists(accountNumber)) {
      System.out.println("Create account successfully");
      accountService.saveAccount(account);
      Pair<String, List<Pair<String, String>>> outer = new ImmutablePair<String, List<Pair<String, String>>>("created",
          innerList);
      result.add(outer);
    } else {
      System.out.println("Account existed");
      Pair<String, List<Pair<String, String>>> outer = new ImmutablePair<String, List<Pair<String, String>>>(
          "Account already existed!", innerList);
      result.add(outer);
    }
  }

  /**
   * This function is to parse order
   * For buy order:
   * 1) check symbol exist, balance is enough
   * 2) deduct $ from the account.
   * 3) find matching orders(sellers should not be buyer) by symbol, order by
   * price from low to high,
   * then by time from past to now
   * 4) handle matching order, deduct shares, if shares is not 0 after all deals,
   * save this order with the
   * remaining shares. With every matching order, deal should be saved, refund
   * money added to the account.
   * 
   * For sell order:
   * 1) check account has enough shares of certain symbol
   * 2) find matching orders(buyer should not be seller) by symbol, order by price
   * from high to low,
   * then by time from past to now
   * 3) handle matching orders
   * 
   * @param node xml node <order>
   * @throws Exception
   */
  public void parseOrder(Long accountNumber, Node node) {
    String symbol = getAttributeValue(node, "sym");
    Integer amount = Integer.parseInt(getAttributeValue(node, "amount"));
    Double limit = Double.parseDouble(getAttributeValue(node, "limit"));

    Element orderElement;
    if (amount == 0) {
      // TODO nothing to do, might report error here
      return;
    } else if (amount < 0) {
      // check account has this symbol and has enough shares
      int absAmount = Math.abs(amount);
      if (positionService.accountHasSymbol(accountNumber, symbol)
          && positionService.accountHasEnoughShares(accountNumber, symbol, absAmount)) {
        Integer sellOrderId = parseSell(accountNumber, symbol, absAmount, limit);
        // <opened sym="SYM" amount="AMT" limit="LMT" id="TRANS_ID"/>
        orderElement = new Element("opened");
        orderElement.setAttribute(new Attribute("sym", symbol));
        orderElement.setAttribute(new Attribute("amount", Integer.toString(amount)));
        orderElement.setAttribute(new Attribute("limit", Double.toString(limit)));
        orderElement.setAttribute(new Attribute("id", Integer.toString(sellOrderId)));

      } else {
        orderElement = new Element("error");
        orderElement.setAttribute(new Attribute("sym", symbol));
        orderElement.setAttribute(new Attribute("amount", Integer.toString(amount)));
        orderElement.setAttribute(new Attribute("limit", Double.toString(limit)));
        if (!positionService.accountHasSymbol(accountNumber, symbol)) {
          orderElement.setText("account does have that symbol");
          System.out.println("Error in sell: account does have that symbol");
        } else if (!positionService.accountHasEnoughShares(accountNumber, symbol, absAmount)) {
          orderElement.setText("account does not have enough shares");
          System.out.println("Error in sell: account does not have enough shares");
        }
      }
    } else {
      if (symbolService.checkSymbolExists(symbol)
          && accountService.accountHasEnoughBalance(accountNumber, amount * limit)) {
        Integer buyOrderId = parseBuy(accountNumber, symbol, amount, limit);
        orderElement = new Element("opened");
        orderElement.setAttribute(new Attribute("sym", symbol));
        orderElement.setAttribute(new Attribute("amount", Integer.toString(amount)));
        orderElement.setAttribute(new Attribute("limit", Double.toString(limit)));
        orderElement.setAttribute(new Attribute("id", Integer.toString(buyOrderId)));
      } else {
        orderElement = new Element("error");
        orderElement.setAttribute(new Attribute("sym", symbol));
        orderElement.setAttribute(new Attribute("amount", Integer.toString(amount)));
        orderElement.setAttribute(new Attribute("limit", Double.toString(limit)));
        if (!symbolService.checkSymbolExists(symbol)) {
          orderElement.setText("symbol not exists");
          System.out.println("Error in buy: symbol not exists");
        } else if (!accountService.accountHasEnoughBalance(accountNumber, amount * limit)) {
          orderElement.setText("account does not have enough balance");
          System.out.println("Error in buy: account does not have enough balance");
        }
      }
    }
    transactionRoot.addContent(orderElement);
  }

  public Integer parseBuy(Long accountNumber, String symbol, Integer amount, Double price) {
    Double moneyToPay = amount * price;
    accountService.deductAccountBalance(accountNumber, moneyToPay);
    List<Order> orders = orderService.selectSellOrdersByPriceAndDate(symbol, accountNumber, price);
    Integer buyerOrderId = orderService.saveOrder(accountNumber, symbol, amount, price, "buy");
    for (Order o : orders) {
      int sellerOrderId = o.getId();
      long sellerAccountNumber = o.getAccountNumber();
      double sellPrice = o.getPrice();
      int sellShares = o.getShares();
      if (amount == sellShares) {
        double money = amount * sellPrice;
        double refund = amount * (price - sellPrice);
        dealService.saveDeal(symbol, amount, sellPrice, buyerOrderId, sellerOrderId);
        accountService.addAccountBalance(sellerAccountNumber, money); // add money to seller's account
        accountService.addAccountBalance(accountNumber, refund); // add refund to buyer's account
        positionService.addSharesBySyAccountAndSymbol(amount, accountNumber, symbol); // buyer get amount of symbol
        orderService.deductOrderShares(buyerOrderId, amount); // update buyer shares
        orderService.deductOrderShares(sellerOrderId, amount); // update seller shares
        orderService.updateOrderStatus(buyerOrderId, "close"); // update buyer order status
        orderService.updateOrderStatus(sellerOrderId, "close"); // update seller order status
        break;
      } else if (amount < o.getShares()) {
        double money = amount * sellPrice;
        double refund = amount * (price - sellPrice);
        dealService.saveDeal(symbol, amount, sellPrice, buyerOrderId, sellerOrderId);
        accountService.addAccountBalance(sellerAccountNumber, money); // add money to seller's account
        accountService.addAccountBalance(accountNumber, refund); // add refund to buyer's account
        positionService.addSharesBySyAccountAndSymbol(amount, accountNumber, symbol); // buyer get amount of symbol
        orderService.deductOrderShares(buyerOrderId, amount); // update buyer shares
        orderService.deductOrderShares(sellerOrderId, amount); // update seller shares
        orderService.updateOrderStatus(buyerOrderId, "close"); // update buyer order status
        break;
      } else {
        double money = o.getShares() * sellPrice;
        double refund = o.getShares() * (price - sellPrice);
        dealService.saveDeal(symbol, sellShares, sellPrice, buyerOrderId, sellerOrderId);
        accountService.addAccountBalance(sellerAccountNumber, money); // add money to seller's account
        accountService.addAccountBalance(accountNumber, refund); // add refund to buyer's account
        positionService.addSharesBySyAccountAndSymbol(sellShares, accountNumber, symbol); // buyer get amount of symbol
        orderService.deductOrderShares(buyerOrderId, sellShares); // update buyer shares
        orderService.deductOrderShares(sellerOrderId, sellShares); // update seller shares
        orderService.updateOrderStatus(sellerOrderId, "close"); // update seller order status
      }
      amount -= o.getShares();
    }
    return buyerOrderId;
  }

  public Integer parseSell(Long accountNumber, String symbol, Integer amount, Double price) {
    positionService.deductSharesBySyAccountAndSymbol(amount, accountNumber, symbol);
    List<Order> orders = orderService.selectBuyOrdersByPriceAndDate(symbol, accountNumber, price);
    // first generate an order
    Integer sellerOrderId = orderService.saveOrder(accountNumber, symbol, amount, price, "sell");

    for (Order o : orders) {
      // amount equals to order's shares, order status change to close, shares to 0,
      if (amount == o.getShares()) {
        Double money = amount * o.getPrice();
        // generate deal, add, deduct moeny, add, deduct shares, update order
        dealService.saveDeal(symbol, amount, o.getPrice(), o.getId(), sellerOrderId);
        accountService.addAccountBalance(accountNumber, money); // seller get money when deal executed
        positionService.addSharesBySyAccountAndSymbol(amount, o.getAccountNumber(), symbol); // buyer get amount of
                                                                                             // symbol
        orderService.deductOrderShares(o.getId(), o.getShares()); // update buyer shares
        orderService.deductOrderShares(sellerOrderId, amount); // update seller shares
        orderService.updateOrderStatus(o.getId(), "close"); // update buyer order status
        orderService.updateOrderStatus(sellerOrderId, "close"); // update seller order status
        break;
      } else if (amount < o.getShares()) {
        Double money = amount * o.getPrice();
        // generate deal, add, deduct moeny, add, deduct shares, update order
        dealService.saveDeal(symbol, amount, o.getPrice(), o.getId(), sellerOrderId);
        accountService.addAccountBalance(accountNumber, money); // seller get money when deal executed
        positionService.addSharesBySyAccountAndSymbol(amount, o.getAccountNumber(), symbol); // buyer get amount of
                                                                                             // symbol
        orderService.deductOrderShares(o.getId(), amount); // update buyer shares
        orderService.deductOrderShares(sellerOrderId, amount); // update seller shares
        // orderService.updateOrderStatus(o.getId(), "close"); //update buyer order
        // status
        orderService.updateOrderStatus(sellerOrderId, "close"); // update seller order status
        break;
      } else { // amount > o.getShares()
        Double money = o.getShares() * o.getPrice();
        // generate deal, add, deduct moeny, add, deduct shares, update order
        dealService.saveDeal(symbol, o.getShares(), o.getPrice(), o.getId(), sellerOrderId);
        accountService.addAccountBalance(accountNumber, money); // seller get money when deal executed
        positionService.addSharesBySyAccountAndSymbol(o.getShares(), o.getAccountNumber(), symbol); // buyer get amount
                                                                                                    // of symbol
        orderService.deductOrderShares(o.getId(), o.getShares()); // update buyer shares
        orderService.deductOrderShares(sellerOrderId, o.getShares()); // update seller shares
        orderService.updateOrderStatus(o.getId(), "close"); // update buyer order status
        // orderService.updateOrderStatus(sellerOrderId, "close"); //update seller order
        // status
      }
      amount -= o.getShares();
    }
    return sellerOrderId;
  }

  /**
   * This function is handle <cancel> tag whose top-level tag <transaction>
   * 
   * @param Long accountNumber, the account to perform the operation
   * @param node xml node <cancel>
   */
  public void parseTransactionCancel(Long accountNumber, Node node) {
    // check tag format
    NamedNodeMap attributes = node.getAttributes();
    Node transactionId = attributes.getNamedItem("id"); // attribute node
    Element cancelElement;
    if (attributes.getLength() != 1 || transactionId == null) {
      cancelElement = new Element("error");
      cancelElement.setText("not a proper format of cancel transaction");
      System.out.println(node + "is not a proper format of cancel transaction");
      transactionRoot.addContent(cancelElement);
      return;
    }

    Integer id = Integer.parseInt(getAttributeValue(node, "id"));
    Order order = orderService.selectOrderById(id);
    if (!order.getStatus().equals("opened")){
      cancelElement = new Element("error");
      cancelElement.setText("This order is not opened and can not be cancled");
      System.out.println("This order is not opened and can not be cancled");
      transactionRoot.addContent(cancelElement);
      return;
    }

    if (orderService.checkOrderExists(accountNumber, id)) {
      orderService.setAsCancled(id);
      List<Deal> deals = dealService.getDealsBasedOnOperationAndOrderId(order.getOperation(), order.getId());
      cancelElement = new Element("canceled");
      cancelElement.setAttribute(new Attribute("id", Integer.toString(id)));
      Element innerCancelElement = new Element("canceled");
      innerCancelElement.setAttribute(new Attribute("shares", Integer.toString(order.getShares())));
      cancelElement.addContent(innerCancelElement);
      for (Deal deal : deals){
        Element dealElement = new Element("executed");
        dealElement.setAttribute(new Attribute("shares", Integer.toString(deal.getShares())));
        dealElement.setAttribute(new Attribute("price", Double.toString(deal.getPrice())));
        dealElement.setAttribute(new Attribute("time", Long.toString(deal.getDate().getTime())));
        cancelElement.addContent(dealElement);
      }
      transactionRoot.addContent(cancelElement);
    } else { // This account doest not have the transaction
      cancelElement = new Element("error");
      cancelElement.setText("This order is not belong to this account");
      transactionRoot.addContent(cancelElement);
      System.out.println("This order is not belong to this account");
      transactionRoot.addContent(cancelElement);
    }
  }

  /**
   * This function is handle <query> tag whose top-level tag <transaction>
   * 1. order has not been executed or canceled:
   * 1）only get oreder info (status, shares)
   * 2. order has been partially executed:
   * 1）get order info (status, shares)
   * 2) get executed deals based on sell or buy (shares, price, time)
   * 3. order is fully executed:
   * 1) get executed deals based on sell or buy (shares, price, time)
   * 
   * @param Long accountNumber, the account to perform the operation
   * @param node xml node <query>
   */
  public void parseQuery(Long accoutNumber, Node node) {
    // check tag format
    NamedNodeMap attributes = node.getAttributes();
    Node transactionId = attributes.getNamedItem("id"); // attribute node

    Element queryElement;
    if (attributes.getLength() != 1 || transactionId == null) {
      // TODO: send error msg?
      queryElement = new Element("error");
      queryElement.setText("not a proper format of query transaction");
      transactionRoot.addContent(queryElement);
      System.out.println("Error in query format!");
      return;
    }

    Integer id = Integer.parseInt(getAttributeValue(node, "id"));
    if (orderService.checkOrderExists(accoutNumber, id)) {

      Order order = orderService.selectOrderById(id); // get order
      System.out.println(order.getShares() + " " + order.getStatus() + " " + order.getDate());

      queryElement = new Element("status");
      queryElement.setAttribute(new Attribute("id", Integer.toString(id)));
      Element openElement = new Element(order.getStatus());
      openElement.setAttribute(new Attribute("shares", Integer.toString(order.getShares())));
      queryElement.addContent(openElement);
      List<Deal> deals = dealService.getDealsBasedOnOperationAndOrderId(order.getOperation(), order.getId());
      for (Deal deal : deals){
        Element dealElement = new Element("executed");
        dealElement.setAttribute(new Attribute("shares", Integer.toString(deal.getShares())));
        dealElement.setAttribute(new Attribute("price", Double.toString(deal.getPrice())));
        dealElement.setAttribute(new Attribute("time", Long.toString(deal.getDate().getTime())));
        queryElement.addContent(dealElement);
      }
      transactionRoot.addContent(queryElement);
    } else {
      queryElement = new Element("error");
      queryElement.setText("account does not have that order");
      transactionRoot.addContent(queryElement);
    }
  }

  /**
   * This function return attribute value based on the attribute name
   * 
   * @param node xml node
   * @param name attribute name
   * @return The value of corresponding attribute
   */
  public String getAttributeValue(Node node, String name) {
    return node.getAttributes().getNamedItem(name).getNodeValue();
  }

  /**
   * helper function to judge if string is all number
   * 
   * @param str
   * @return true if str is all number
   */
  public boolean isNumeric1(String str) {
    Pattern pattern = Pattern.compile("-?[0-9]*");
    return pattern.matcher(str).matches();
  }

}
