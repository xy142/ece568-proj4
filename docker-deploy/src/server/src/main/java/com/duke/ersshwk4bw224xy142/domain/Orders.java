package com.duke.ersshwk4bw224xy142.domain;

import java.util.Date;

public class Orders {
    Integer orderId;
    String symName;
    Double amount;
    Double limitPrice;
    Integer status; // 0(open),1(canceled), 2(closed)
    Date time;
    Integer accId;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getSymName() {
        return symName;
    }

    public void setSymName(String symName) {
        this.symName = symName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getLimitPrice() {
        return limitPrice;
    }

    public void setLimitPrice(Double limitPrice) {
        this.limitPrice = limitPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getAccId() {
        return accId;
    }

    public void setAccId(Integer accId) {
        this.accId = accId;
    }

    @Override
    public String toString() {
        return "Orders [accId=" + accId + ", amount=" + amount + ", limitPrice=" + limitPrice + ", orderId=" + orderId
                + ", symName=" + symName + ", time=" + time + "]";
    }

}
