package com.duke.ersshwk4bw224xy142.domain;

import lombok.*;

public class Accounts {
    int accId;
    double balance;

    public Accounts(int accId, double balance) {
        this.accId = accId;
        this.balance = balance;
    }

    public int getaccId() {
        return accId;
    }

    public void setaccId(int accId) {
        this.accId = accId;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "Accounts [accId=" + accId + ", balance=" + balance + "]";
    }

    // @Override
    // public int hashCode() {
    // final int prime = 31;
    // int result = 1;
    // result = prime * result + ((accId == null) ? 0 : accId.hashCode());
    // result = prime * result + ((balance == null) ? 0 : balance.hashCode());
    // return result;
    // }

}
