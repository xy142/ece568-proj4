package com.duke.ersshwk4bw224xy142.domain;

public class Positions {
    Integer accId;
    String symName;
    Double shares;

    // public Integer getId() {
    // return id;
    // }

    // public void setId(Integer id) {
    // this.id = id;
    // }

    public Integer getaccId() {
        return accId;
    }

    public void setaccId(Integer accId) {
        this.accId = accId;
    }

    public String getsysName() {
        return symName;
    }

    public void setsymName(String symName) {
        this.symName = symName;
    }

    public Double getShares() {
        return shares;
    }

    public void setShares(Double shares) {
        this.shares = shares;
    }

    @Override
    public String toString() {
        return "Positions [accId=" + accId + ", shares=" + shares + ", symName=" + symName + "]";
    }

}
