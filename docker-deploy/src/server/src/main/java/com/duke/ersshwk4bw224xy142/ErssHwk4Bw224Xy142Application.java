package com.duke.ersshwk4bw224xy142;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import com.duke.ersshwk4bw224xy142.Utils.AsyncService;
import com.duke.ersshwk4bw224xy142.Utils.XmlParser;
import com.duke.ersshwk4bw224xy142.Utils.connectionHandler;
import com.duke.ersshwk4bw224xy142.service.AccountService;

import org.jdom2.*;
import org.jdom2.output.*;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@MapperScan("com.duke.ersshwk4bw224xy142.mapper")
@SpringBootApplication
@EnableTransactionManagement
public class ErssHwk4Bw224Xy142Application implements CommandLineRunner {
	private final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
	// private final AccountService accountService;
	private final XmlParser xmlParser;
	// private connectionHandler connHandler = null;
	@Autowired
	private AsyncService asynService;

	@Autowired
	public ErssHwk4Bw224Xy142Application(AccountService accountService, XmlParser xmlParser) {
		// this.accountService = accountService;
		this.xmlParser = xmlParser;
	}

	// no thread-version server
	public void serverStartNoThread() throws IOException {
		ServerSocket ss = new ServerSocket(12345);
		System.out.println("start to accept...");
		for (;;) {
			Socket sock = ss.accept();
			System.out.println("connected from " + sock.getRemoteSocketAddress());
			connectionHandler connHandler = new connectionHandler(sock);
			try {
				String request = connHandler.readXML();
				// System.out.println(request);
				// try (PrintWriter out = new PrintWriter("./test.txt")) {
				// out.println(request);
				// }
				// parse and hanle request
				Document resp = xmlParser.parseRequest(request);
				String xmlRespStr = xmlOutputter.outputString(resp);
				System.out.println("Finish handlind");
				System.out.println("Send below back to client");
				System.out.println(xmlRespStr);
				connHandler.sendXML(xmlRespStr);
				// once send this response, close the sock
				sock.close();
			} catch (Exception e) {
				try {
					sock.close();
				} catch (IOException ioe) {
					e.printStackTrace();
					System.out.println("Can't close sock");
				}
				e.printStackTrace();
				System.out.println("client disconnected.");
			}

		}
	}

	/**
	 * One thread hanlde one sokcet, i.e. one client
	 * Theoretically, it would be much efficient if same client send a lots reqeust
	 * as no need for reconnection and thread switching
	 * 
	 * @throws IOException
	 */
	public void serverStartWithThreadForSock() throws IOException {
		ServerSocket ss = new ServerSocket(12345);
		System.out.println("start to accept...");
		for (;;) {
			Socket sock = ss.accept();
			// here create a new thread
			// one thread handle one socket, untill the user disconnect the connection
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					System.out.println("connected from " + sock.getRemoteSocketAddress());
					System.out.println(
							"_____________________thread " + Thread.currentThread().getId() + "___________________");
					try {
						handleOneSock(sock);
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						System.out.println("Thread " + Thread.currentThread().getId() + " ends");
					}
				}
			});
			thread.start();
		}
	}

	private void handleOneSock(Socket sock) throws IOException {
		connectionHandler connHandler = new connectionHandler(sock);
		for (;;) {
			try {
				String request = connHandler.readXML();
				// System.out.println(request);
				// try (PrintWriter out = new PrintWriter("./test.txt")) {
				// out.println(request);
				// }
				// parse and hanle request
				if(request.equals("Wrong Format: The first line is not a number")){
					continue;
				}
				if (request.equals("BYE")) {
					System.exit(0);
				}
				Document resp = xmlParser.parseRequest(request);
				String xmlRespStr = xmlOutputter.outputString(resp);
				// System.out.println("Finish handlind");
				// System.out.println("Send below back to client");
				// System.out.println(xmlRespStr);
				connHandler.sendXML(xmlRespStr);
			} catch (Exception e) {
				try {
					sock.close();
				} catch (IOException ioe) {
					e.printStackTrace();
					System.out.println("Can't close sock");
					break;
				}
				e.printStackTrace();
				System.out.println("client disconnected.");
				break;
			}
		}
	}

	// one sokcet only for one time receive and send: hanlde one request, send back,
	// close the that socket
	public void serverStartWithThreadForRequest() throws IOException, ClassNotFoundException {
		ServerSocket ss = new ServerSocket(12345);
		for (;;) {
			System.out.println("start to accept...");
			Socket sock = ss.accept();
			Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					System.out.println("connected from " + sock.getRemoteSocketAddress());
					System.out.println(
							"_____________________thread " + Thread.currentThread().getId() + "___________________");
					try {
						connectionHandler connHandler = new connectionHandler(sock);
						String request = connHandler.readXML();
						// System.out.println(request);
						// try (PrintWriter out = new PrintWriter("./test.txt")) {
						// out.println(request);
						// }
						// parse and hanle request

						Document resp = xmlParser.parseRequest(request);

						String xmlRespStr = xmlOutputter.outputString(resp);
						System.out.println("Finish handlind");
						System.out.println("Send below back to client");
						System.out.println(xmlRespStr);
						connHandler.sendXML(xmlRespStr);
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("client disconnected.");

					} finally {
						try {
							sock.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.println("!!!!!!The server disconnect the sock!!!!!!!!");
						System.out.println("Thread " + Thread.currentThread().getId() + " ends");
					}
				}
			});
			thread.start();

		}
	}

	// after hanlde one request, then close the that socket
	public void serverStartWithThreadForRequestBySpringPool() throws IOException, JDOMException {
		ServerSocket ss = new ServerSocket(12345);
		for (;;) {
			System.out.println("start to accept...");
			Socket sock = ss.accept();
			asynService.handleOneSokcetByPoll(sock);
		}
	}

	// 4th, poll(receive+ add to queue) + thread handle task queue

	// try {
	// // we have multiThreads working on each request
	// //asynService.handleOneRequest(connHandler, requestQ);
	// } catch (Exception e) {
	// e.printStackTrace();
	// System.out.println("The client disconnect");
	// }
	// }
	// });

	public static void main(String[] args) {
		SpringApplication.run(ErssHwk4Bw224Xy142Application.class, args);
		System.exit(0);
	}

	@Override
	public void run(String... args) throws Exception {
		// xmlParser.readRequest("/home/bw224/erss-hwk4-bw224-xy142/src/main/resources/xmls/create.txt");
		//
		// Document doc = xmlParser
		// .parseRequest("/home/bw224/erss-hwk4-bw224-xy142/src/main/resources/xmls/transactionOrder.txt");
		// Element root = doc.getRootElement();

		// TODO: add length to the response
		/**
		 * return String:
		 * <?xml version="1.0" encoding="UTF-8"?>
		 * <results>
		 * <error id="2">Account already exists</error>
		 * </results>
		 * need to subString the first line
		 */
		// xmlOutputter.outputString(doc);

		// choose the model you want
		int model = 2;
		// int model = Integer.parseInt(args[0]);

		if (model == 1) {
			serverStartNoThread();
		} else if (model == 2) {
			serverStartWithThreadForSock();
		} else if (model == 3) {
			serverStartWithThreadForRequest();
		} else if (model == 4) {
			serverStartWithThreadForRequestBySpringPool();
		}
	}

}
