package com.duke.ersshwk4bw224xy142.mapper;

import java.util.List;

import com.duke.ersshwk4bw224xy142.domain.Deals;

import org.springframework.stereotype.Repository;

@Repository
public interface DealsMapper {

    public List<Deals> selectDealsById(int transacId);

    public void addDeal(Deals deal);
}
