package com.duke.ersshwk4bw224xy142.mapper;

import java.util.List;

import com.duke.ersshwk4bw224xy142.domain.Symbols;

import org.springframework.stereotype.Repository;

@Repository
public interface SymbolsMapper {
    public List<Symbols> selectSymByName(String symName);

    public void addSymbol(String symName);

    public Symbols symbolExists(String symName);
}
