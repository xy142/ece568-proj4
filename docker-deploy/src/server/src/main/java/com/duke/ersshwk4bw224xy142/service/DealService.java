package com.duke.ersshwk4bw224xy142.service;

import java.util.*;

import com.duke.ersshwk4bw224xy142.Utils.XmlGenerator;
import com.duke.ersshwk4bw224xy142.domain.Deals;
import com.duke.ersshwk4bw224xy142.domain.Orders;
import com.duke.ersshwk4bw224xy142.mapper.DealsMapper;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DealService {
    @Autowired
    DealsMapper dealsMapper;

    public List<Deals> selectDealsById(int transacId) {
        return dealsMapper.selectDealsById(transacId);
    }

    public void addDeal(int buyerId, Orders orderBuy, Orders orderSell, boolean buySide) {
        Deals deal = new Deals();
        Date time = new Date();
        double price = 0;
        if (buySide) {
            price = orderSell.getLimitPrice();
        } else {
            price = orderBuy.getLimitPrice();
        }
        // the amount of transacton is the min
        double amount = Math.min(orderBuy.getAmount(), Math.abs(orderSell.getAmount()));
        // ERROR
        deal.setBuyerOrderId(orderBuy.getOrderId());
        deal.setSellerOrderId(orderSell.getOrderId());
        deal.setTime(time);
        deal.setAmount(amount);
        deal.setPrice(price);
        deal.setSymName(orderBuy.getSymName());
        System.out.println(deal.toString());

        dealsMapper.addDeal(deal);

    }
}
