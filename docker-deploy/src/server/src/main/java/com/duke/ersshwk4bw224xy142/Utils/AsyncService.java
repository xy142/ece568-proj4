package com.duke.ersshwk4bw224xy142.Utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Queue;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.jdom2.output.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {
    @Autowired
    private XmlParser xmlParser;
    private final XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());

    // use threads in the doSomethingExecutor thread pool to do this task
    @Async("doSomethingExecutor")
    public void handleOneSokcetByPoll(Socket sock) throws IOException, JDOMException {
        // while()
        // System.out.println(
        // "_____________________thread " + Thread.currentThread().getId() +
        // "___________________");
        // Document resp = xmlParser.parseRequest("./test.txt");
        // String xmlRespStr = xmlOutputter.outputString(resp);
        // System.out.println("Finish handlind");
        // System.out.println("Send below back to client");
        // System.out.println(xmlRespStr);
        // connHandler.sendXML(xmlRespStr);
        // connHandler.close();
        // //Thread.sleep(1000);
        // System.out.println("Thread " + Thread.currentThread().getId() + " ends");
        // System.out.println("connected from " + sock.getRemoteSocketAddress());
        System.out.println(
                "_____________________thread " + Thread.currentThread().getId() +
                        "___________________");
        try {
            connectionHandler connHandler = new connectionHandler(sock);
            String request = connHandler.readXML();
            if (request.equals("BYE")) {
                System.out.println("BYE");
                System.exit(0);
            }
            // System.out.println(request);
            // try (PrintWriter out = new PrintWriter("./test.txt")) {
            // out.println(request);
            // }
            // parse and hanle request
            // Document resp = xmlParser.parseRequest("./test.txt");
            for (;;) {
                Document resp = xmlParser.parseRequest(request);
                String xmlRespStr = xmlOutputter.outputString(resp);
                // System.out.println("Finish handlind");
                // System.out.println("Send below back to client");
                // System.out.println(xmlRespStr);
                connHandler.sendXML(xmlRespStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("client disconnected.");

        } finally {
            try {
                sock.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // System.out.println("!!!!!!The server disconnect the sock!!!!!!!!");
            System.out.println("Thread " + Thread.currentThread().getId() + " ends");
        }
    }
}
