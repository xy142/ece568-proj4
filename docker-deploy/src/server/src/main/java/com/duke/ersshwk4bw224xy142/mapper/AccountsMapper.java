package com.duke.ersshwk4bw224xy142.mapper;

import java.util.List;

import com.duke.ersshwk4bw224xy142.domain.Accounts;

import org.springframework.stereotype.Repository;

@Repository
public interface AccountsMapper {
    public Accounts selectAccountById(Integer accId);

    public void addAccount(int accId, double balance); // accId auto increase？

    public Accounts accHasEnoughBalance(int accId, double balance);

    public void updateBalance(int accId, double balance);

}
