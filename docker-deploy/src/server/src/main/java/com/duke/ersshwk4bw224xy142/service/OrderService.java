// 0(open),1(canceled), 2(closed)
package com.duke.ersshwk4bw224xy142.service;

import java.text.SimpleDateFormat;
import java.util.*;

import com.duke.ersshwk4bw224xy142.Utils.XmlGenerator;
import com.duke.ersshwk4bw224xy142.Utils.customException;
import com.duke.ersshwk4bw224xy142.domain.Orders;
import com.duke.ersshwk4bw224xy142.mapper.OrdersMapper;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    OrdersMapper ordersMapper;

    public Orders selectOrderByAccAndId(int accId, int orderId) {
        return ordersMapper.selectOrderByAccAndId(accId, orderId);
    }

    // public void addOrder(String symName, double amount, double limitPrice,
    // int status, Date time, int accId) {
    // ordersMapper.addOrder(accId, amount, limitPrice, status, time, symName);
    // }

    public Orders selectOpenOrderByAccAndId(int accId, int orderId) {
        return ordersMapper.selectOpenOrderByAccAndId(accId, orderId);
    }

    public List<Orders> getPotentialSellOrders(Orders order) {
        return ordersMapper.getPotentialSellOrders(order);
    }

    public List<Orders> getPotentialBuyOrders(Orders order) {
        return ordersMapper.getPotentialBuyOrders(order);
    }

    public void updateOrderStatus(int orderId, String operation) throws customException {
        int status = 0;
        switch (operation) {
            case "cancel":
                status = 1;
                break;
            case "open":
                status = 0;
                break;
            case "close":
                status = 2;
                break;
            default:
                throw new customException("invalid operation");
        }
        ordersMapper.updateOrderStatus(orderId, status);
    }

    public void updateOrderShares(int orderId, double diffAmount) {
        ordersMapper.updateOrderShares(orderId, diffAmount);
    }

    public int addOrder(int accId, String symName, Double amount, Double limitPrice) {
        int status = 0; // open
        Date time = new Date();
        // System.out.println("time of the order is: " + time.getTime() / 1000);
        Orders order = new Orders();
        order.setAccId(accId);
        order.setAmount(amount);
        order.setLimitPrice(limitPrice);
        order.setStatus(status);
        order.setSymName(symName);
        order.setTime(time);
        ordersMapper.addOrder(order);
        return order.getOrderId();
    }

}