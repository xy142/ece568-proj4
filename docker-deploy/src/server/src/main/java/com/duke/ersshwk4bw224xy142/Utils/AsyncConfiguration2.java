package com.duke.ersshwk4bw224xy142.Utils;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncConfiguration2 {

    @Bean("hanldeTheRequest")
    public Executor hanldeTheRequest() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        // the initial thread numbers
        executor.setCorePoolSize(10);
        // the max thread numbers
        executor.setMaxPoolSize(20);
        // waiting queue
        // executor.setQueueCapacity(500);
        // ruin this thread after 60s
        // executor.setKeepAliveSeconds(60);
        executor.initialize();
        return executor;
    }

}