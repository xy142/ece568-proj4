package com.duke.ersshwk4bw224xy142.service;

import java.util.List;

import com.duke.ersshwk4bw224xy142.domain.Accounts;
import com.duke.ersshwk4bw224xy142.domain.Symbols;
import com.duke.ersshwk4bw224xy142.mapper.AccountsMapper;
import com.duke.ersshwk4bw224xy142.mapper.SymbolsMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SymbolService {
    @Autowired
    SymbolsMapper symbolsMapper;

    public void addSymbol(String symName) {
        symbolsMapper.addSymbol(symName);
    }

    public boolean symbolExists(String symName) {
        if (symbolsMapper.symbolExists(symName) != null) {
            return true;
        }
        return false;
    }
}
