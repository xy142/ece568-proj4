package com.duke.ersshwk4bw224xy142.Utils;

import java.util.*;
import org.apache.commons.lang3.StringUtils;
import com.duke.ersshwk4bw224xy142.domain.Deals;
import com.duke.ersshwk4bw224xy142.domain.Orders;

import org.jdom2.*;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.tuple.*;

@Service
public class XmlGenerator {
    // List<Pair<created/errmsg, List<Pair<Attr, Value>>>>
    // private final List<Pair<String, List<Pair<String, String>>>> response;

    // private Element result;
    // private Document response;

    // public XmlGenerator() {
    // // this.response = response;
    // result = new Element("results");
    // response = new Document(result);
    // }

    /**
     * add created msg to <results>
     * 
     * @param prefix:     created for success, error content for error
     * @param createdMsg: List<Pair<attr name, attr value>>
     */
    public void addToResp(String prefix, List<Pair<String, String>> Attrs) {
        // response.add(new ImmutablePair<String, List<Pair<String, String>>>(prefix,
        // Attrs));
    }

    // generate one response for <creat><account>
    public Element geneCreateAccResp(int accId, String msg) {
        // elment name
        String name = msg.equals("created") ? "created" : "error";
        Element accResp = new Element(name);
        accResp.setAttribute("id", Integer.toString(accId));
        if (!msg.equals("created")) {
            accResp.setText(msg);
        }
        return accResp;
    }

    // generate one response for <create><symbol>
    public Element geneCreateSymResp(String symName, int accId, String msg) {
        String name = msg.equals("created") ? "created" : "error";
        Element symResp = new Element(name);
        List<Attribute> attrs = new ArrayList<>();
        attrs.add(new Attribute("sym", symName));
        attrs.add(new Attribute("id", Integer.toString(accId)));
        symResp.setAttributes(attrs);
        if (!msg.equals("created")) {
            symResp.setText(msg);
        }
        return symResp;
    }

    /**
     * Generate one response item for <order>
     * 
     * @param order
     * @return orderResp: <opened> / <error>
     * @throws customException
     */
    public Element geneOrderResp(Orders order, String msg) throws customException {
        Element orderResp;
        int orderId = order.getOrderId();
        List<Attribute> orderAttrs = new ArrayList<>();
        orderAttrs.add(new Attribute("sym", order.getSymName()));
        orderAttrs.add(new Attribute("amount", Double.toString(order.getAmount())));
        orderAttrs.add(new Attribute("limit", Double.toString(order.getLimitPrice())));
        // orderId > 0: opened
        if (orderId > 0 && msg.equals("opened")) {
            orderAttrs.add(new Attribute("id", Integer.toString(orderId)));
            orderResp = new Element(msg);
            orderResp.setAttributes(orderAttrs);
        }
        // orderId = 0: error
        else if (orderId == 0 && !msg.equals("opened")) {
            orderResp = new Element("error");
            orderResp.setAttributes(orderAttrs);
            orderResp.setText(msg);
        } else {
            throw new customException("Illegal value of orderId");
        }
        return orderResp;
    }

    /**
     * Generate error response for <transaction>
     */
    public Element geneErrorResp(String attr, String msg) {
        Element errorResp = new Element("error");
        if (StringUtils.isNumeric(attr))
            errorResp.setAttribute("id", attr);
        else if (attr != "")
            errorResp.setAttribute("sym", attr);
        errorResp.setText(msg);
        // result.addContent(queryResp);
        return errorResp;
    }

    public Element geneErrorResp(String sym, int id, String msg) {
        Element errorResp = new Element("error");
        errorResp.setAttribute("sym", sym);
        errorResp.setAttribute("id", Integer.toString(id));
        errorResp.setText(msg);
        // result.addContent(queryResp);
        return errorResp;
    }

    /**
     * 
     * @param deals
     * @param parent
     * @return
     */
    public Element geneDeals(List<Deals> deals, Element parent) {
        for (Deals deal : deals) {
            Element executed = new Element("executed");
            List<Attribute> attrs = new ArrayList<>();
            attrs.add(new Attribute("shares", Double.toString(deal.getAmount())));
            attrs.add(new Attribute("price", Double.toString(deal.getPrice())));
            attrs.add(new Attribute("time", Long.toString(deal.getTime().getTime() / 1000)));
            executed.setAttributes(attrs);
            parent.addContent(executed);
        }
        return parent;
    }

    /**
     * Generate one response item for <query>
     * 
     * @param transacId
     * @param orders
     * @param deals
     * @return queryResp: <status id =""> / <error>
     * @throws customException
     */
    public Element geneQueryResp(int transacId, Orders order, List<Deals> deals) {
        Element queryResp = new Element("status");
        queryResp.setAttribute("id", Integer.toString(transacId));
        double share = order.getAmount();
        Attribute shares = new Attribute("shares", Double.toString(share));
        if (order.getStatus() == 0) { // open
            Element open = new Element("open");
            open.setAttribute(shares);
            queryResp.addContent(open);
        } else if (order.getStatus() == 1) { // canceled
            Long time = order.getTime().getTime() / 1000;
            Element canceled = new Element("canceled");
            canceled.setAttribute(shares);
            canceled.setAttribute("time", Long.toString(time));
            queryResp.addContent(canceled);
        }
        return geneDeals(deals, queryResp);
        // for (Deals deal : deals) {
        // Element executed = new Element("executed");
        // List<Attribute> attrs = new ArrayList<>();
        // attrs.add(new Attribute("shares", Double.toString(deal.getAmount())));
        // attrs.add(new Attribute("price", Double.toString(deal.getPrice())));
        // attrs.add(new Attribute("time", Long.toString(deal.getTime().getTime() /
        // 1000)));
        // executed.setAttributes(attrs);
        // queryResp.addContent(executed);
        // }
        // return queryResp;
    }

    public Element geneCancelResp(int transacId, Orders order, List<Deals> deals) {
        Element cancelResp = new Element("canceled");
        cancelResp.setAttribute("id", Integer.toString(transacId));
        Element canceled = new Element("canceled");
        double shares = order.getAmount();
        Long time = order.getTime().getTime() / 1000;
        canceled.setAttribute("shares", Double.toString(shares));
        canceled.setAttribute("time", Long.toString(time));
        cancelResp.addContent(canceled);
        if (!deals.isEmpty()) {
            geneDeals(deals, cancelResp);
        }
        return cancelResp;
    }

}
