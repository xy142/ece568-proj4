package com.duke.ersshwk4bw224xy142.mapper;

import java.util.*;

import com.duke.ersshwk4bw224xy142.domain.Orders;

import org.springframework.stereotype.Repository;

@Repository
public interface OrdersMapper {
    public Orders selectOrderByAccAndId(int accId, int orderId);

    public List<Orders> getPotentialSellOrders(Orders order);

    public List<Orders> getPotentialBuyOrders(Orders order);

    // public void addOrder(int accId, double amount, double limitPrice,
    // int status, Date time, String symName);

    public Orders selectOpenOrderByAccAndId(int accId, int orderId);

    public void updateOrderStatus(int orderId, int status);

    public void updateOrderShares(int orderId, double diffAmount);

    public void addOrder(Orders order);;

}