package com.duke.ersshwk4bw224xy142.service;

import java.util.List;

import com.duke.ersshwk4bw224xy142.domain.Positions;
import com.duke.ersshwk4bw224xy142.mapper.PositionsMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PositionService {
    @Autowired
    PositionsMapper positionsMapper;

    public void addSymToAcc(Positions position) {
        int accId = position.getaccId();
        String sym = position.getsysName();
        Double shares = position.getShares();
        // account has this symbol, update the owned shares
        if (positionsMapper.accHasSymbol(accId, sym) != null && shares >= 0) {
            positionsMapper.updateSharesByAccAndSym(accId, sym, shares);
            // account doesn't has this symbol, create new position
        } else {
            positionsMapper.addSymToAcc(accId, sym, shares);

        }
    }

    public boolean accHasSymbol(int accId, String sym) {
        Positions position = positionsMapper.accHasSymbol(accId, sym);
        if (position != null) {
            return true;
        }
        return false;
    }

    public void updateSharesByAccAndSym(int accId, String sym, double shares) {
        positionsMapper.updateSharesByAccAndSym(accId, sym, shares);
    }

    public Positions selectByAccShares(int accId, String sym, Double shares) {
        System.out.println("shares need to deduct: " + Math.abs(shares));
        System.out.println(
                "shares I have: " + positionsMapper.selectByAccShares(accId, sym, Math.abs(shares)).getShares());
        return positionsMapper.selectByAccShares(accId, sym, Math.abs(shares));
    }
}
