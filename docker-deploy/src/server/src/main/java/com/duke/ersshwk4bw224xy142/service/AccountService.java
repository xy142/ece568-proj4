package com.duke.ersshwk4bw224xy142.service;

import java.util.*;

import com.duke.ersshwk4bw224xy142.Utils.XmlGenerator;
import com.duke.ersshwk4bw224xy142.domain.Accounts;
import com.duke.ersshwk4bw224xy142.mapper.AccountsMapper;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {
    @Autowired
    AccountsMapper accountsMapper;

    public Accounts selectAccountById(int accId) {
        return accountsMapper.selectAccountById(accId);
    }

    public void addAccount(Accounts account) {
        accountsMapper.addAccount(account.getaccId(), account.getBalance());
    }

    public void updateBalance(int accId, double balance) {
        accountsMapper.updateBalance(accId, balance);
    }
    
    public boolean checkAccExist(int accId) {
        if (selectAccountById(accId) != null) {
            return true;
        }
        return false;

    }

    public boolean accHasEnoughBalance(int accId, double balance) {
        if (accountsMapper.accHasEnoughBalance(accId, balance) != null) {
            return true;
        }
        return false;
    }

    // /**
    // * @param exist: true to validate whether the acc exist, false to validate the
    // * acc not exist
    // * @param accId
    // * @param Attr
    // * @return
    // */
    // TODO: change it
    // public boolean checkAccExist(boolean exist, int accId, List<Pair<String,
    // String>> Attrs) {
    // // List<Pair<String, String>> Attrs = new ArrayList<>();
    // // Attrs.add(Attr);

    // // System.out.println("account id = " + selectAccountById(accId).getaccId());
    // // debug
    // System.out.println("_______________________DEBUG_________________");
    // System.out.println("selectAccountById(accId)= " + selectAccountById(accId));
    // if (selectAccountById(accId) != null && exist == true) {
    // System.out.println("Account already exists\n");
    // xmlGenerator.addToResp("Account already existes", Attrs);
    // return true;
    // } else if (selectAccountById(accId) == null && exist == false) {
    // Pair<String, String> accAttr = new ImmutablePair<String, String>("id",
    // Integer.toString(accId));
    // Attrs.add(accAttr);
    // System.out.println("Account not exists\n");
    // xmlGenerator.addToResp("Account not exists", Attrs);
    // return true;
    // }
    // return false;
    // }

}
