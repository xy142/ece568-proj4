package com.duke.ersshwk4bw224xy142.domain;

import java.util.Date;

public class Deals {
    Integer id;
    Integer buyerOrderId;
    Integer sellerOrderId;
    String symName;
    Double amount;
    Double price;
    Date time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBuyerOrderId() {
        return buyerOrderId;
    }

    public void setBuyerOrderId(Integer buyerOrderId) {
        this.buyerOrderId = buyerOrderId;
    }

    public Integer getSellerOrderId() {
        return sellerOrderId;
    }

    public void setSellerOrderId(Integer sellerOrderId) {
        this.sellerOrderId = sellerOrderId;
    }

    public String getSymName() {
        return symName;
    }

    public void setSymName(String symName) {
        this.symName = symName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Deals [amount=" + amount + ", buyerOrderId=" + buyerOrderId + ", id=" + id + ", price=" + price
                + ", sellerOrderId=" + sellerOrderId + ", symName=" + symName + ", time=" + time + "]";
    }

}
