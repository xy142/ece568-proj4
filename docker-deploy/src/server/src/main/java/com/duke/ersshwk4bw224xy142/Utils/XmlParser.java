package com.duke.ersshwk4bw224xy142.Utils;

import org.apache.commons.lang3.tuple.*;
import org.jdom2.*;
import org.jdom2.input.SAXBuilder;

import java.util.*;

import javax.naming.spi.DirStateFactory.Result;

import com.duke.ersshwk4bw224xy142.domain.Accounts;
import com.duke.ersshwk4bw224xy142.domain.Orders;
import com.duke.ersshwk4bw224xy142.domain.Positions;
import com.duke.ersshwk4bw224xy142.service.AccountService;
import com.duke.ersshwk4bw224xy142.service.PositionService;
import com.duke.ersshwk4bw224xy142.service.SymbolService;

import java.io.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class XmlParser {

    @Autowired
    AccountService accountService;
    @Autowired
    PositionService positionService;
    @Autowired
    SymbolService symbolService;
    @Autowired
    XmlGenerator xmlGenerator;
    @Autowired
    RequestHandler requestHandler;

    public Element accParser(Element account) throws DataConversionException {
        Accounts Account = new Accounts(0, 0.0);
        int accId = account.getAttribute("id").getIntValue();
        String msg = "";
        if (!accountService.checkAccExist(accId)) {
            // If not exist, then created
            msg = "created";
            Account.setaccId(accId);
            Account.setBalance(account.getAttribute("balance").getDoubleValue());
            accountService.addAccount(Account);
            System.out.println("Create account successfully\n");
        } else { // error
            msg = "Account already exists";
        }
        return xmlGenerator.geneCreateAccResp(accId, msg);
    }

    public List<Element> symParser(Element symbol) throws DataConversionException {
        // each <symbol><account> has one corresponing response
        List<Element> responses = new ArrayList<>();
        String symName = symbol.getAttribute("sym").getValue();

        List<Element> symAccountList = symbol.getChildren("account");
        if (!symAccountList.isEmpty()) {
            for (Element account : symAccountList) {
                String msg = "";
                int accId = account.getAttribute("id").getIntValue();
                if (accountService.checkAccExist(accId)) {
                    // the account should exist, otherwise response with error
                    msg = "created";
                    try {
                        double shares = Double.parseDouble(account.getText());
                        if (shares <= 0) {
                            responses.add(xmlGenerator.geneErrorResp(symName, accId,
                                    "Illegal share value: non-positive shares"));
                            continue;
                        }
                        symbolService.addSymbol(symName); // add symbol if not exist
                        Positions position = new Positions();
                        position.setaccId(accId);
                        position.setsymName(symName);
                        position.setShares(shares);
                        positionService.addSymToAcc(position);

                    } catch (NumberFormatException e) {
                        responses.add(xmlGenerator.geneErrorResp(symName, accId,
                                "Illegal share value: the share is not double"));
                    }
                } else {
                    // the account does not exist, return error
                    msg = "Account does not exist";
                }
                Element accResp = xmlGenerator.geneCreateSymResp(symName, accId, msg);
                responses.add(accResp);
            }
            return responses;
        } else {
            responses.add(xmlGenerator.geneErrorResp(symName, "No subchild for symbol"));
            return responses;
        }
    }

    /**
     * parse top-level tag <create>
     * 
     * @param root
     * @throws DataConversionException
     * @throws customException
     */
    public Document createParser(Element root) throws DataConversionException {
        Element rootResponse = new Element("results");
        Document response = new Document(rootResponse);
        List<Element> children = root.getChildren();
        for (Element child : children) {
            // Element childResponse = null;
            if (child.getName().equals("account")) {
                rootResponse.addContent(accParser(child));

            } else if (child.getName().equals("symbol")) {
                for (var resp : symParser(child)) {
                    rootResponse.addContent(resp);
                }
            } else {
                rootResponse.addContent(xmlGenerator.geneErrorResp("", "Illegal child of create"));
            }
        }
        return response;
    }

    /**
     * parse top-level tag <transactions>
     * 
     * @param root
     * @return
     * @throws DataConversionException
     * @throws customException
     */
    public Document transcsParser(Element root) throws DataConversionException {
        // this the response for transction
        Element rootResponse = new Element("results");
        Document response = new Document(rootResponse);

        Attribute idNode = root.getAttribute("id");
        if (idNode == null) {
            rootResponse.addContent(xmlGenerator.geneErrorResp("", "No account id for transaction"));
            return response;
        }
        int accId = idNode.getIntValue();
        boolean accValid = accountService.checkAccExist(accId);
        // the account_id should exist, otherwise return error
        List<Element> children = root.getChildren();

        // TODO: generate error response for each child in <trasaction>

        if (!children.isEmpty()) {
            for (Element child : children) {
                Element childResponse = requestHandler.transcsHandle(child, accId, accValid);
                // each child is the children of root of response
                rootResponse.addContent(childResponse);
            }
            return response;
        } else {
            rootResponse.addContent(
                    xmlGenerator.geneErrorResp(
                            Integer.toString(accId), "No subchild for transaction root"));
            return response;
        }
    }

    /**
     * 
     * @param fileName
     * @return response Document if success; null if failed
     * @throws IOException
     * @throws JDOMException
     */
    // parse and handle
    // read String xml
    // @Async("hanldeTheRequest")
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, timeout = 36000, rollbackFor = Exception.class)
    public Document parseRequest(String request) throws IOException, JDOMException {
        BufferedReader br = new BufferedReader(new StringReader(request));
       // br.readLine();
        SAXBuilder sb = new SAXBuilder();
        Document docJdom = null;
        try {
            docJdom = sb.build(br);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Document response = null;
        Element root = docJdom.getRootElement();
        try {
            if (root.getName() == "create") {
                response = createParser(root);
            } else if (root.getName() == "transactions") {
                response = transcsParser(root);
            } else {
                System.out.println("Illegal root name\n");
            }
            // generate and send back
            // return xmlGenerator.geneResponse();
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
        } finally {
            br.close();
            // System.out.println(
            // "_____________________thread " + Thread.currentThread().getId()
            // + " finish handling request from this client___________________");
        }
        return null;
    }

}
