package com.duke.ersshwk4bw224xy142.Utils;

import java.util.*;

import com.duke.ersshwk4bw224xy142.domain.Deals;
import com.duke.ersshwk4bw224xy142.domain.Orders;
import com.duke.ersshwk4bw224xy142.domain.Positions;
import com.duke.ersshwk4bw224xy142.service.AccountService;
import com.duke.ersshwk4bw224xy142.service.DealService;
import com.duke.ersshwk4bw224xy142.service.OrderService;
import com.duke.ersshwk4bw224xy142.service.PositionService;
import com.duke.ersshwk4bw224xy142.service.SymbolService;

import org.jdom2.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.val;

@Service
public class RequestHandler {
    @Autowired
    XmlGenerator xmlGenerator;
    @Autowired
    PositionService positionService;
    @Autowired
    AccountService accountService;
    @Autowired
    SymbolService symbolService;
    @Autowired
    OrderService orderService;
    @Autowired
    DealService dealService;

    /**
     * insert order, receive order_id
     * 
     * @param accId
     * @param order
     * @return
     */
    public int addOrder(int accId, Orders order) {
        String sym = order.getSymName();
        Double amount = order.getAmount();
        Double price = order.getLimitPrice();
        return orderService.addOrder(accId, sym, amount, price);
    }

    /**
     * insert order, update table Accounts (deduct balance), seek for match
     * 
     * @param accId
     * @param order
     * @return
     * @throws customException
     */
    public int buyHandle(int accId, Orders order) throws customException {
        // insert order, receive order_id
        int orderId = addOrder(accId, order);
        order.setOrderId(orderId);
        // update balance in account table(deduct account's balance)
        // NOTE: the real price will get changed when executed: update the diff if
        // matched
        accountService.updateBalance(accId, -1 * order.getAmount() * order.getLimitPrice());

        // seek for match
        // Do something: uptading the opened order shares
        tryMatchBuySide(accId, order);
        return orderId;
    }

    /**
     * insert order, update position(deduct share), seek for match
     * 
     * @param accId
     * @param order
     * @return
     * @throws customException
     */
    public int sellHandle(int accId, Orders order) throws customException {
        int orderId = addOrder(accId, order);
        // ERROR: should not be update a order, create a new order because time, also
        // there may many same symbol orders
        // update position(deduct share)
        // TODO:
        order.setOrderId(orderId);
        positionService.updateSharesByAccAndSym(accId, order.getSymName(), order.getAmount());

        // seek for match
        tryMatchSellSide(accId, order);

        return orderId;
    }

    public boolean tryMatchBuySide(int accId, Orders order) throws customException {
        boolean ifExecuted = false;
        // 1. get all the according open stocks for sell side, as it is the buy-side, we
        // look at the stocks of sell side from low prics to high
        List<Orders> potentials = orderService.getPotentialSellOrders(order);
        // 2. find the first price that lower than our price
        for (var orderSell : potentials) {
            if (orderSell.getLimitPrice() <= order.getLimitPrice()) {
                ifExecuted = true;
                // we can execute these two orders
                // 1.update the transaction in deals
                dealService.addDeal(accId, order, orderSell, true);
                // 2. update the seller balance: add money
                // amount * realPrice
                double price = orderSell.getLimitPrice();
                double amount = Math.min(Math.abs(orderSell.getAmount()), order.getAmount());
                accountService.updateBalance(orderSell.getAccId(), amount * price);
                // 3. update the buyer balance: return the money left
                // amount * (limitPrice - realPrice)>0
                accountService.updateBalance(accId, amount * (order.getLimitPrice() - price));
                // 4.update the buyer shares in Position
                Positions position = new Positions();
                position.setaccId(accId);
                position.setsymName(order.getSymName());
                position.setShares(amount);
                positionService.addSymToAcc(position);

                // now check the amount for the Ordersell
                if (Math.abs(orderSell.getAmount()) > order.getAmount()) {
                    // update the orderSell shares in Orders: deduce buyer order amount
                    orderService.updateOrderShares(orderSell.getOrderId(), amount);
                    // close the order in the Orders
                    orderService.updateOrderStatus(order.getOrderId(), "close");
                    break;
                } else if (Math.abs(orderSell.getAmount()) == order.getAmount()) {
                    // two sides properly match, close both, end trymatch
                    orderService.updateOrderStatus(orderSell.getOrderId(), "close");
                    orderService.updateOrderStatus(order.getOrderId(), "close");
                    break;
                } else { // buyer has more shares, continue to check
                         // close the sellOrder in the orders
                    orderService.updateOrderStatus(orderSell.getOrderId(), "close");
                    // update the order shares in Orders
                    orderService.updateOrderShares(order.getOrderId(), -1 * amount);

                    // update the amount of order, to match next sellOrder
                    order.setAmount(orderService.selectOrderByAccAndId(accId, order.getOrderId()).getAmount());

                }
            }
        }
        return ifExecuted;
    }

    public boolean tryMatchSellSide(int accId, Orders order) throws customException {
        boolean ifExecuted = false;
        // 1. get all the according open stocks for buy side, as it is the sell-side
        // sell order, we
        // look at the stocks of sell side from high price to low
        // ERROR?: the ties are sorted by time
        List<Orders> potentials = orderService.getPotentialBuyOrders(order);
        // 2. find the first price that lower than our price
        for (var orderBuy : potentials) {
            if (orderBuy.getLimitPrice() >= order.getLimitPrice()) {
                ifExecuted = true;
                // we can execute these two orders
                // 1.update the transaction in deals
                dealService.addDeal(accId, orderBuy, order, false);
                // 2. update the seller balance: add money
                // amount * realPrice
                double price = orderBuy.getLimitPrice();
                double amount = Math.min(orderBuy.getAmount(), Math.abs(order.getAmount()));
                accountService.updateBalance(order.getAccId(), amount * price);
                // 3. update the buyer balance: return the money left
                // amount * (limitPrice - realPrice)=0
                accountService.updateBalance(accId, amount * (orderBuy.getLimitPrice() - price));
                // 4.update the buyer shares in Position
                Positions position = new Positions();
                position.setaccId(orderBuy.getAccId());
                position.setsymName(order.getSymName());
                position.setShares(amount);
                positionService.addSymToAcc(position);

                // now check the amount for the Ordersell
                // seller has more shares, continue to check
                if (Math.abs(order.getAmount()) > orderBuy.getAmount()) {
                    // update the orderSell shares in Orders: deduce sell order amount
                    orderService.updateOrderShares(order.getOrderId(), amount);
                    // close the order in the Orders
                    orderService.updateOrderStatus(orderBuy.getOrderId(), "close");
                    // update the amount of order, to match next buyOrder
                    order.setAmount(orderService.selectOrderByAccAndId(accId, order.getOrderId()).getAmount());
                } else if (Math.abs(order.getAmount()) == orderBuy.getAmount()) {
                    // two sides properly match, close both, end trymatch
                    orderService.updateOrderStatus(order.getOrderId(), "close");
                    orderService.updateOrderStatus(orderBuy.getOrderId(), "close");
                    break;
                } else { // no more shares for seller
                         // close the sellOrder in the orders
                    orderService.updateOrderStatus(order.getOrderId(), "close");
                    // update the order shares in Orders
                    orderService.updateOrderShares(orderBuy.getOrderId(), -1 * amount);
                    break;
                }
            }
        }
        return ifExecuted;
    }

    /**
     * <order sys="" amount="" limit="" /> -> order
     * 
     * @param orderEle
     * @return
     * @throws customException
     */
    public Orders parseOrderAttrs(Element orderEle) throws customException {
        Orders order = new Orders();
        List<Attribute> attrList = orderEle.getAttributes();
        if (attrList.size() != 3) {
            throw new customException("Illegal num of attrs");
        }
        // parse three attributes value(sym, amount, limit) into order
        for (Attribute attr : attrList) {
            String attrName = attr.getName();
            String value = attr.getValue();
            switch (attrName) {
                case "sym":
                    order.setSymName(value);
                    break;
                case "amount":
                    order.setAmount(Double.valueOf(value));
                    break;
                case "limit":
                    order.setLimitPrice(Double.valueOf(value));
                    break;
                default:
                    throw new customException("Illegal attr of order");
            }
        }
        return order;
    }

    /**
     * order format: <order sym="SPY" amount="100" limit="145.67"/>
     * 
     * @param element: the order element
     * @param accId:   the accId owned this order
     * @throws customException
     */
    public Element orderHandle(Element orderEle, int accId, boolean accValid) {
        Element eleToAdd = null;
        try {
            Orders order = parseOrderAttrs(orderEle);
            order.setAccId(accId);
            order.setOrderId(0);
            double amount = order.getAmount();
            String sym = order.getSymName();
            double limit = order.getLimitPrice();
            if (accValid) {
                if (amount == 0) {
                    return xmlGenerator.geneOrderResp(order, "The amount should not be 0");
                }
                // buy order
                else if (amount > 0) {
                    // check the validity of this buy order
                    // 1. symbol exists
                    // System.out.println("need balance: " + amount * limit);
                    // System.out.println("has balance: " +
                    // accountService.selectAccountById(accId).getBalance());
                    // System.out.println(accountService.accHasEnoughBalance(accId, amount *
                    // limit));
                    if (!symbolService.symbolExists(sym)) {
                        eleToAdd = xmlGenerator.geneOrderResp(order, "symbol to buy does not exist");
                    }
                    // 2. account has enough balance to buy

                    else if (!accountService.accHasEnoughBalance(accId, amount * limit)) {
                        eleToAdd = xmlGenerator.geneOrderResp(order, "insufficient account balance");
                    } else {
                        order.setOrderId(buyHandle(accId, order));
                        // set amount to the original one, incase partially executed
                        order.setAmount(amount);
                        eleToAdd = xmlGenerator.geneOrderResp(order, "opened");
                    }
                }
                // sell order
                else {
                    // check the validity of this sell order
                    // 1. account has no symbol to sell: no position / position's share = 0
                    if (!positionService.accHasSymbol(accId, sym)) {
                        eleToAdd = xmlGenerator.geneOrderResp(order, "account does not have this symbol");
                    }
                    // 2. account has not enough shares to sell
                    else if (positionService.selectByAccShares(accId, sym, amount) == null) {
                        eleToAdd = xmlGenerator.geneOrderResp(order, "insufficient share of this symbol");
                    } else {
                        order.setOrderId(sellHandle(accId, order));
                        // set amount to the original one, incase partially executed
                        order.setAmount(amount);
                        eleToAdd = xmlGenerator.geneOrderResp(order, "opened");
                    }
                }
            } else {
                // if account id is not valid, generate <error> directly
                eleToAdd = xmlGenerator.geneOrderResp(order, "Invalid account Id");
            }
        } catch (Exception e) {
            return xmlGenerator.geneErrorResp(Integer.toString(accId), e.getMessage());
        }
        return eleToAdd;
    }

    public Element queryHandle(Element queryEle, int accId, boolean accValid) throws DataConversionException {
        int transacId = queryEle.getAttribute("id").getIntValue();
        if (accValid) {
            // check this accId have this order
            Orders order = orderService.selectOrderByAccAndId(accId, transacId);
            if (order != null) {
                List<Deals> deals = dealService.selectDealsById(transacId);
                return xmlGenerator.geneQueryResp(transacId, order, deals);
            } else {
                return xmlGenerator.geneErrorResp(Integer.toString(transacId), "No such order for this account");
            }
        } else {
            return xmlGenerator.geneErrorResp(Integer.toString(transacId), "Invalid account Id");
        }
    }

    /**
     * handle <cancel> tag of <transaction>
     * 
     * @param cancelEle
     * @param accId
     * @param accValid
     * @throws DataConversionException
     * @throws customException
     */
    public Element cancelHandle(Element cancelEle, int accId, boolean accValid)
            throws DataConversionException {
        int transacId = cancelEle.getAttribute("id").getIntValue();
        if (accValid) {
            Orders order = orderService.selectOrderByAccAndId(accId, transacId);
            if (order != null) {
                if (order.getStatus() == 0) { // open order, valid
                    // update the order status from open to cancel
                    try {
                        orderService.updateOrderStatus(transacId, "cancel");
                    } catch (Exception e) {
                        return xmlGenerator.geneErrorResp(Integer.toString(transacId), e.getMessage());
                    }
                    if (order.getAmount() > 0) { // if a buy order, refund buyer's account
                        double balance = order.getAmount() * order.getLimitPrice();
                        accountService.updateBalance(accId, balance);
                    } else { // if a sell order, return shares to seller's account
                        double shares = order.getAmount();
                        positionService.updateSharesByAccAndSym(accId, order.getSymName(), shares);
                    }
                    List<Deals> deals = dealService.selectDealsById(transacId);
                    return xmlGenerator.geneCancelResp(transacId, order, deals);
                } else { // canceled or closed order, invalid
                    return xmlGenerator.geneErrorResp(Integer.toString(transacId), "Unopen order: cannot be canceled!");
                }
            } else {
                return xmlGenerator.geneErrorResp(Integer.toString(transacId), "No such order for this account");
            }
        } else {
            return xmlGenerator.geneErrorResp(Integer.toString(transacId), "Invalid account Id");
        }
    }

    public Element transcsHandle(Element child, int accId, boolean accValid)
            throws DataConversionException {
        if (child.getName().equals("order")) {
            return orderHandle(child, accId, accValid);
        } else if (child.getName().equals("query")) {
            return queryHandle(child, accId, accValid);
        } else if (child.getName().equals("cancel")) {
            return cancelHandle(child, accId, accValid);
        } else {
            return xmlGenerator.geneErrorResp(Integer.toString(accId), "Illegal child of transaction");
        }
    }
}
