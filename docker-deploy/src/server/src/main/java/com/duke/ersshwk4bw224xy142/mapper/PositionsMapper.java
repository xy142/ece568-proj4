package com.duke.ersshwk4bw224xy142.mapper;

import java.util.List;

import com.duke.ersshwk4bw224xy142.domain.Positions;

import org.springframework.stereotype.Repository;

@Repository
public interface PositionsMapper {
    public void addSymToAcc(int accId, String symName, double shares);

    public Positions accHasSymbol(int accId, String sym);

    // ERROR?: have many postions entries update which postions entries
    public void updateSharesByAccAndSym(int accId, String sym, double shares);

    public Positions selectByAccShares(int accId, String sym, double shares);
}
