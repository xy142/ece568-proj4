package com.duke.ersshwk4bw224xy142.Utils;

public class customException extends Exception {

    // 无参构造方法
    public customException() {

        super();
    }

    // 有参的构造方法
    public customException(String message) {
        super(message);

    }

    // 用指定的详细信息和原因构造一个新的异常
    public customException(String message, Throwable cause) {

        super(message, cause);
    }

    // 用指定原因构造一个新的异常
    public customException(Throwable cause) {

        super(cause);
    }

}
