package com.duke.ersshwk4bw224xy142.Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class connectionHandler {
    private final Socket sock;
    private final BufferedReader in;
    private final BufferedWriter out;

    public connectionHandler(Socket sock) throws IOException {
        this.sock = sock;
        out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream(), StandardCharsets.UTF_8));
        in = new BufferedReader(new InputStreamReader(sock.getInputStream(), StandardCharsets.UTF_8));
        
    }

    // public String readXML() throws ClassNotFoundException, IOException {
    //         StringBuilder sb = new StringBuilder();
    //         String line;
    //         while ((line = in.readLine()) != null){
    //             sb.append(line).append("\n");
    //         }
    //         return sb.toString();
    // }

    public String readXML() throws ClassNotFoundException, IOException{
        String line = in.readLine();
        if(line == null){
            throw new RuntimeException("client close the conncetion");
        }
        int length;
        try{
            length = Integer.parseInt(line);
            if(length <=0){
                throw new RuntimeException("lenght should be positive");
            }
        }
        catch(NumberFormatException e){
            return "Wrong Format: The first line is not a number";            
        }
       // System.out.println("lenth of the command is " + length);
        char[] chars = new char[length];
        int charsRead = in.read(chars, 0, length);
        String result;
        if (charsRead != -1) {
            result = new String(chars, 0, charsRead);
        } else {
            throw new RuntimeException( "client close the conncetion");
        }

        return result;
    }

    public void sendXML(String XMLToSend) throws IOException {
        int length = XMLToSend.length() + 10;
        String msg = length + "\n" + XMLToSend;
        out.write(msg);
        out.flush();
    }

    public void close() throws IOException{
        this.sock.close();
    }

}
