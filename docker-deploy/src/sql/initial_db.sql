/*
 Navicat Premium Data Transfer

 Source Server         : ece568
 Source Server Type    : PostgreSQL
 Source Server Version : 120009
 Source Host           : vcm-25953.vm.duke.edu:5432
 Source Catalog        : ece568hw4
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 120009
 File Encoding         : 65001

 Date: 31/03/2022 14:59:17
*/


DROP SEQUENCE IF EXISTS public.accounts_acc_id_seq CASCADE;

CREATE SEQUENCE public.accounts_acc_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

DROP SEQUENCE IF EXISTS public.deals_id_seq CASCADE;

CREATE SEQUENCE public.deals_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

DROP SEQUENCE IF EXISTS public.orders_order_id_seq CASCADE;

CREATE SEQUENCE public.orders_order_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;

DROP SEQUENCE IF EXISTS public.positions_id_seq CASCADE;

CREATE SEQUENCE public.positions_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;-- public."Accounts" definition

-- Drop table

DROP TABLE IF EXISTS public."Accounts" CASCADE;

CREATE TABLE public."Accounts" (
	acc_id int4 NOT NULL,
	balance float8 NOT NULL,
	CONSTRAINT account_pk PRIMARY KEY (acc_id)
);


-- public."Symbols" definition

-- Drop table

DROP TABLE IF EXISTS public."Symbols" CASCADE;

CREATE TABLE public."Symbols" (
	sym_name varchar NOT NULL,
	CONSTRAINT symbols_pk PRIMARY KEY (sym_name)
);


-- public."Orders" definition

-- Drop table

DROP TABLE IF EXISTS public."Orders" CASCADE;

CREATE TABLE public."Orders" (
	order_id int4 NOT NULL DEFAULT nextval('orders_order_id_seq'::regclass),
	sym_name varchar NOT NULL,
	amount float8 NOT NULL,
	limit_price float8 NOT NULL,
	status int4 NOT NULL, -- 0(open),1(canceled), 2(closed)
	"time" timestamp NOT NULL,
	acc_id int4 NOT NULL,
	CONSTRAINT orders_pk PRIMARY KEY (order_id),
	CONSTRAINT orders_acc_fk FOREIGN KEY (acc_id) REFERENCES public."Accounts"(acc_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT orders_fk FOREIGN KEY (sym_name) REFERENCES public."Symbols"(sym_name) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Column comments

COMMENT ON COLUMN public."Orders".status IS '0(open),1(canceled), 2(closed)';


-- public."Positions" definition

-- Drop table

DROP TABLE IF EXISTS public."Positions" CASCADE;

CREATE TABLE public."Positions" (
	id int4 NOT NULL DEFAULT nextval('positions_id_seq'::regclass),
	acc_id int4 NOT NULL,
	sym_name varchar NOT NULL,
	shares float8 NOT NULL,
	CONSTRAINT positions_pk PRIMARY KEY (id),
	CONSTRAINT positions_acc_fk FOREIGN KEY (acc_id) REFERENCES public."Accounts"(acc_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT positions_fk FOREIGN KEY (sym_name) REFERENCES public."Symbols"(sym_name) ON DELETE CASCADE ON UPDATE CASCADE
);


-- public."Deals" definition

-- Drop table

DROP TABLE IF EXISTS public."Deals" CASCADE;

CREATE TABLE public."Deals" (
	id int4 NOT NULL DEFAULT nextval('deals_id_seq'::regclass),
	"buyerOrder_id" int4 NOT NULL,
	"sellerOrder_id" int4 NOT NULL,
	amount float8 NOT NULL,
	price float8 NOT NULL,
	"time" date NOT NULL,
	sym_name varchar NOT NULL,
	CONSTRAINT deals_pk PRIMARY KEY (id),
	CONSTRAINT deals_fk FOREIGN KEY ("buyerOrder_id") REFERENCES public."Orders"(order_id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT deals_fk_1 FOREIGN KEY ("sellerOrder_id") REFERENCES public."Orders"(order_id)
);
