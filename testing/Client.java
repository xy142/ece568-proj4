import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Client {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("start to connect to server");
        // Socket sock = new Socket(args[0], 12345);
        Socket sock = new Socket("localhost", 12345);
        try {
            System.out.println("connected!");
            InputStream input = sock.getInputStream();
            OutputStream output = sock.getOutputStream();
            handle(input, output);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("The server closes connection");
        } finally {
            sock.close();
        }
    }

    private static void handle(InputStream input, OutputStream output) throws IOException, ClassNotFoundException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8));
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
        for (int i = 0; i < 5000; i++) {
            try (BufferedReader br = new BufferedReader(new FileReader("./request.txt"))) {
                String line;
                StringBuilder lines = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    // writer.write(line);
                    lines.append(line);
                    lines.append('\n');
                    // System.out.println(line);
                }
                String msgStr = lines.toString();
                int length = lines.toString().length()+10;
                String msg = length + "\n" + msgStr;
                System.out.println(msg);
                System.out.println("I'm writting back");
                writer.write(msg);
                writer.flush();

                System.out.println("SEND SUCCESFULLY");
                String reponse = readXML(reader);
                // StringBuilder sb = new StringBuilder();
                // String lineRead;
                // while ((lineRead = reader.readLine()) != null) {
                //     sb.append(lineRead).append("\n");
                // }
                // String reponse = sb.toString();
                System.out.println("Receive the msg back");
                System.out.println("Receiving....");
                System.out.println(reponse);
                // String response = (String) reader.readObject();
            }
        }
        System.out.println("__________FINSIH SENDING______________");
    }

    public static String readXML(BufferedReader in) throws ClassNotFoundException, IOException {
        String line = in.readLine();
        if(line == null){
            throw new IOException();
        }
        int length = Integer.parseInt(line);
        System.out.println("lenth of the command is " + length);
        char[] chars = new char[length];
        int charsRead = in.read(chars, 0, length);
        String result;
        if (charsRead != -1) {
            result = new String(chars, 0, charsRead);
        } else {
            throw new RuntimeException("server close the conncetion");
        }

        return result;
    }






}