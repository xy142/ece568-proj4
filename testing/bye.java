import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * this class is only for test use, to automatically let server go down
 */
public class bye {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // System.out.println("start to connect to server");
        Socket sock = new Socket(args[0], 12345);
        try {
            // System.out.println("connected!");
            // InputStream input = sock.getInputStream();
            OutputStream output = sock.getOutputStream();
            ObjectOutputStream writer = new ObjectOutputStream(output);
            writer.writeObject("BYE");
            System.out.println("BYE_____________________________________1");
            // handle(input, output);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("The server closes connection");
        } finally {
            sock.close();
        }
    }
}
