For server starting:
    1. delete the whole postgres-data directory under the ./erss*./docker-deploy
    2. Run 'docker-compose up' under the directory './erss*./docker-deploy'
    
For testing: 
    1. 'cd testing'
    2. Run 'bash test.sh' under the directory ./testing.
        The first arg should be the number of clients you want,
        the second ard should be the address of your virtual machine( e.g. vcm-xxxxx.vm.duke.edu)
    3. to change the number of requests for each client you want send to the server, chaneg the value i in the for loops;e.g. i<1000 representing 1000

